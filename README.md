# PrettyNoemieCms

CMS offrant à ses utilisateur une solution ergonomique, simple et élégante pour construire en un rien de temps des sites vitrines responsives au design moderne.


la construction de votre site consistera à agencer à votre convenance des modules variés, d'éditer leurs contenus, et de personnaliser votre site en choisisant les polices de caractère, la mise en forme du texte, ainsi que les couleurs d'affichage

##**[Démo](https://demo-pretty-noemie.frama.site/login)**


login : pretty     
mdp : 12345678


## Grâce à ses modules variés, il conviendra parfaitement à de nombreuses utilisations :

 - Réalisation d'un CV en ligne
 - Portfolio d'artiste ou de créateur
 - Vitrine de votre association
 - Présentation de votre travail d'artisan
 - Page d'accueil d'un festival
 - Et tout ce que votre créativité en fera ...

## Utilisé par Framasoft pour construire les Pages Framasite

Vous pouvez créer gratuitement et librement votre framasite ici : 
[https://frama.site/](https://frama.site/)

## Soutenir
Pour soutenir le créateur de ce projet libre et open source, vous pouvez faire une donation sur Patreon : 

 - [https://www.patreon.com/robinbanquo](https://www.patreon.com/robinbanquo)

